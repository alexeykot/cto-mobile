/**
 * @format
 */

import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import App from './src/screens/Slider';
import { name as appName } from './app.json';
import 'react-native-gesture-handler';

import React from 'react';
import TabBar from './src/components/TabBar';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import configureStore from './src/redux/configureStore';

import Main from './src/screens/Main';
import WebViewPage from './src/screens/WebViewPage';
import OilPage from './src/screens/OilPage';
import MapPage from './src/screens/MapPage';
import Auth from './src/screens/Auth';
import GaragePage from './src/screens/GaragePage';
import ProfilePage from './src/screens/ProfilePage';
import ServicePage from './src/screens/ServicePage';
// import EventScreen from './src/screens/EventScreen';
// import WebViewPage from './src/screens/WebViewScreen';
// import ArchiveScreen from './src/screens/ArchiveScreen';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const store = configureStore();

function Tabs() {
  return (
    <Tab.Navigator tabBar={props => <TabBar {...props} />} headerMode="none">
      <Tab.Screen name="Main" component={Main} />
      <Tab.Screen name="MapPage" component={MapPage} />
      <Tab.Screen name="GaragePage" component={GaragePage} />
      <Tab.Screen name="OilPage" component={OilPage} />
      <Tab.Screen name="ConnectPage" component={OilPage} />
    </Tab.Navigator>
  );
}
function MainApp() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="App" component={App} />
          <Stack.Screen name="Tabs" component={Tabs} />
          <Stack.Screen name="Auth" component={Auth} />
          <Stack.Screen name="Profile" component={ProfilePage} />
          <Stack.Screen name="ServicePage" component={ServicePage} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

AppRegistry.registerComponent(appName, () => MainApp);
