import React from 'react';
import { View, Text, TouchableOpacity, Keyboard } from 'react-native';
import call from '../../assets/images/call.png';
import garage from '../../assets/images/garage.png';
import main from '../../assets/images/main.png';
import oil from '../../assets/images/oil.png';
import stations from '../../assets/images/stations.png';
import * as S from './styled';

const config = [
  {
    name: 'Главная',
    icon: main,
    route: 'Main',
  },
  {
    name: 'Станции',
    icon: stations,
    route: 'MapPage',
    url: 'http://g-service.dev-vps.ru/mobile/ru-RU/map',
  },
  {
    name: 'Гараж',
    icon: garage,
    route: 'GaragePage',
  },
  {
    name: 'Масло',
    icon: oil,
    route: 'OilPage',
    url: 'http://g-service.dev-vps.ru/mobile/ru-RU/oil',
  },
  {
    name: 'Связь',
    icon: call,
    route: 'ConnectPage',
  },
];

const TabBar = props => {
  const [isKeyboardVisible, setKeyboardVisible] = React.useState(false);
  React.useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true); // or some other action
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false); // or some other action
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);
  console.log('props', props.state);
  return (
    <>
      {!isKeyboardVisible && (
        <S.Container>
          {config.map((route, index) => (
            <S.OptionButton
              onPress={() => props.navigation.navigate(route.route)}
              isFocused={props.state.index === index}>
              <S.Icon source={route.icon} />
              <S.OptionText>{route.name}</S.OptionText>
            </S.OptionButton>
          ))}
        </S.Container>
      )}
    </>
  );
};

export default TabBar;
