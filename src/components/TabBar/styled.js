import styled from 'styled-components';

export const Container = styled.View`
  height: 60px;
  width: 100%;
  border-radius: 10px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  background-color: white;
  flex-direction: row;
  padding: 0 30px;
  justify-content: space-between;
`;

export const Icon = styled.Image`
  align-self: center;
  margin-bottom: 8px;
`;

export const OptionButton = styled.TouchableOpacity`
  align-self: center;
  max-width: 48px;
`;

export const OptionText = styled.Text`
  font-size: 12px;
  text-align: center;
  color: black;
`;
