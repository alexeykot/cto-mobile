/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { BoxShadow } from 'react-native-shadow';

import { connect } from 'react-redux';
import logo from '../../assets/images/logo.png';
import call from '../../assets/images/telephone.png';
import profile from '../../assets/images/profile.png';
import bg from '../../assets/images/bg1.png';
import table from '../../assets/images/table.png';
import lock from '../../assets/images/lock.png';
import http from '../../services/http';

import * as S from './styled';

const shadowOpt = {
  width: 330,
  height: 35,
  color: '#e9530c',
  // border: 2,
  radius: 9,
  opacity: 0.1,
  x: -10,
  y: 0,
  style: { marginVertical: 5, height: 150, width: '100%' },
};

class Main extends React.Component {
  state = {
    stock: [],
    advice: [],
  };

  async componentDidMount() {
    const stockData = await http.get(
      'http://g-service.dev-vps.ru/mobile/api/v1/stock',
    );
    this.setState({ stock: stockData.data.data });

    const adviceData = await http.get(
      'http://g-service.dev-vps.ru/mobile/api/v1/advice',
    );
    this.setState({ advice: adviceData.data.data });
  }

  onProfileClick = () => {
    if (this.props.token) {
      this.props.navigation.navigate('Profile');
    } else {
      this.props.navigation.navigate('Auth');
    }
  };

  onGarageClick = () => {
    if (this.props.token) {
      this.props.navigation.navigate('GaragePage');
    } else {
      this.props.navigation.navigate('Auth');
    }
  };
  render() {
    const step = 0;
    return (
      <>
        <S.Container isRegistration={step > 0 && step < 3}>
          <>
            <S.Bg source={bg} />
            {(step === 0 || step === 3) && (
              <>
                <S.HeaderContainer>
                  <S.Header>
                    <S.ProfileButton onPress={this.onProfileClick}>
                      <S.ButtonIcon source={profile} />
                    </S.ProfileButton>
                    <S.Logo source={logo} />
                    <S.TelephoneButton>
                      <S.ButtonIcon source={call} />
                    </S.TelephoneButton>
                  </S.Header>
                  {step === 0 ? (
                    // <BoxShadow setting={shadowOpt}>
                    <S.GarageButton style={{ elevation: 20 }}>
                      <S.TableImage source={table} />
                      <S.GarageTitle>Мой гараж</S.GarageTitle>
                      <S.GarageDescription>
                        Бонусы, история обслуживания, спецпредложения и многое
                        другое
                      </S.GarageDescription>
                      <S.OpenButton onPress={this.onGarageClick}>
                        <S.LockImage source={lock} />
                        <S.OpenButtonText>Открыть сейчас</S.OpenButtonText>
                      </S.OpenButton>
                    </S.GarageButton>
                  ) : (
                    // </BoxShadow>
                    <>
                      <S.HeaderTitle>Ваш гараж</S.HeaderTitle>
                      <S.CarCard>
                        <S.GarageText>Toyota RAV4</S.GarageText>
                      </S.CarCard>
                    </>
                  )}
                </S.HeaderContainer>
                <S.Block>
                  <S.Title color="white">Акции</S.Title>
                  {this.state.stock.length ? (
                    <S.ScrollContainer horizontal>
                      {this.state.stock.map(item => (
                        <S.StockCard>
                          <S.StockImage source={{ uri: item.image }} />
                        </S.StockCard>
                      ))}
                    </S.ScrollContainer>
                  ) : null}
                </S.Block>
                <S.Block isMargined>
                  <S.Title color="black">Полезные советы</S.Title>
                  {this.state.advice.length ? (
                    <S.ScrollContainer horizontal>
                      {this.state.advice.map(item => (
                        <S.SmallCard>
                          <S.AdviceImage
                            source={{ uri: item.image }}
                            imageStyle={{ borderRadius: 6 }}>
                            <S.AdviceTitle>{item.title}</S.AdviceTitle>
                          </S.AdviceImage>
                        </S.SmallCard>
                      ))}
                    </S.ScrollContainer>
                  ) : null}
                </S.Block>
              </>
            )}
          </>
        </S.Container>
        <S.ServiceButton
          onPress={() => this.props.navigation.navigate('ServicePage')}>
          <S.ServiceButtonText>Записаться на сто</S.ServiceButtonText>
        </S.ServiceButton>
      </>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(Main);
