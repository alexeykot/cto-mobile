import styled from 'styled-components';

export const Container = styled.ScrollView`
  flex: 1;
  padding: ${({ isRegistration }) => (isRegistration ? '70px 45px 0 45px' : 0)};
`;

export const Logo = styled.Image`
  width: 122px;
  height: 22px;
`;

export const Bg = styled.Image`
  position: absolute;
  top: 140px;
  left: 0;
  right: 0;
  width: 100%;
  height: 317px;
`;
export const HeaderContainer = styled.View`
  width: 100%;
  padding: 27px 25px 0 25px;
`;

export const Header = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 40px;
  align-items: center;
`;

export const TelephoneButton = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  background-color: #e9530c;
  justify-content: center;
  align-items: center;
`;

export const LockImage = styled.Image`
  width: 14px;
  height: 16px;
  margin-right: 8px;
`;

export const TableImage = styled.Image`
  position: absolute;
  bottom: 0;
  align-self: center;
  width: 260;
  height: 109;
`;
export const ButtonIcon = styled.Image``;

export const ProfileButton = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  background-color: #f4f6f7;
  justify-content: center;
  align-items: center;
`;

export const GarageButton = styled.View`
  width: 100%;
  height: 150px;
  padding: 15px 20px;
  border-radius: 4px;
  background-color: white;
  align-items: center;
  justify-content: space-between;
`;

export const GarageTitle = styled.Text`
  color: black;
  font-weight: 700;
  font-size: 16px;
  text-transform: uppercase;
  color: #e9530c;
`;

export const GarageDescription = styled.Text`
  font-size: 14px;
  color: #1e1e1e;
  text-align: center;
`;

export const OpenButton = styled.TouchableOpacity`
  width: 159px;
  height: 32px;
  border-radius: 16px;
  border: 2px solid #e9530c;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const OpenButtonText = styled.Text`
  color: #e9530c;
  font-size: 12px;
`;

export const CarCard = styled.TouchableOpacity`
  height: 114px;
  background-color: #c4c4c4;
`;

export const Block = styled.View`
  width: 100%;
  margin-top: 26px;
  margin-bottom: ${({ isMargined }) => (isMargined ? '100px' : 0)};
`;

export const Title = styled.Text`
  padding-left: 23px;
  font-size: 18px;
  color: ${({ color }) => color};
  font-weight: 700;
`;

export const ScrollContainer = styled.ScrollView`
  width: 100%;
  margin-top: 23px;
`;

export const StockCard = styled.TouchableOpacity`
  width: 300px;
  height: 240px;
  background-color: #c4c4c4;
  margin-left: 23px;
  border-radius: 6px;
`;

export const StockImage = styled.Image`
  flex: 1;
  border-radius: 6px;
`;

export const SmallCard = styled.TouchableOpacity`
  width: 143px;
  height: 134px;
  background-color: #c4c4c4;
  margin-left: 23px;
  border-radius: 5px;
`;

export const AdviceImage = styled.ImageBackground`
  flex: 1;
  padding: 0 20px 12px 13px;
  align-items: flex-end;
  flex-direction: row;
`;

export const AdviceTitle = styled.Text`
  font-size: 12px;
  color: white;
  font-weight: 700;
`;

export const RegistrationText = styled.Text`
  font-size: 18px;
  color: black;
  margin-bottom: 40px;
`;

export const InputContainer = styled.View`
  width: 100%;
`;

export const InputText = styled.Text`
  font-size: 14px;
  color: black;
`;

export const Input = styled.TextInput`
  height: 42px;
  background-color: #c4c4c4;
  width: 100%;
`;

export const HeaderTitle = styled.Text`
  font-size: 13px;
  color: black;
  margin-top: 25px;
`;

export const BonusCard = styled.View`
  width: 100%;
  height: 84px;
  background-color: #c4c4c4;
  justify-content: center;
  align-items: center;
`;

export const BonusText = styled.Text`
  font-size: 36px;
  color: black;
`;

export const ServiceButton = styled.TouchableOpacity`
  background-color: #e9530c;
  width: 180px;
  height: 50px;
  border-radius: 25px;
  position: absolute;
  bottom: 20;
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const ServiceButtonText = styled.Text`
  color: white;
  font-size: 14px;
  text-transform: uppercase;
`;
