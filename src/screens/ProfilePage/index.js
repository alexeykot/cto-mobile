import React from 'react';
import { connect } from 'react-redux';

import WebViewPage from '../WebViewPage';

const ProfilePage = props => {
  const { token } = props;
  return (
    <WebViewPage
      title="Мой профиль"
      url={`http://g-service.dev-vps.ru/mobile/ru-RU/cabinet/default/index?access-token=${token}`}
    />
  );
};

const mapStateToProps = ({ auth }) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(ProfilePage);
