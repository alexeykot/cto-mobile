/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import TextInputMask from 'react-native-text-input-mask';
import CountDown from 'react-native-countdown-component';

import authStrings from '../../assets/strings/authStrings';

import { requestSaveToken } from '../../redux/auth';

import * as S from './styled';
import http from '../../services/http';
import { connect } from 'react-redux';

class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    step: 0,
    formattedNumber: '',
    code: [undefined, undefined, undefined, undefined],
    resendAvailable: false,
    isNumberInputFocused: false,
    extracted: '',
  };

  onInputChange = async text => {
    const { code } = this.state;
    this.codeLength = text.length;
    const index = text.length - 1;
    console.log(text[index]);
    code[index] = text[index];
    this.setState({ code: code });
    if (text.length === 4) {
      try {
        await http.post(
          'http://g-service.dev-vps.ru/mobile/api/v1/cabinet/verify/phone-verify',
          {
            entity: this.state.extracted,
            code: text,
          },
        );
        const { data } = await http.post(
          'http://g-service.dev-vps.ru/mobile/api/v1/cabinet/authorization/index',
          {
            phone: this.state.extracted,
            code: text,
            os: 'android',
            token: this.state.extracted,
          },
        );
        this.props.requestSaveToken(data.entity['access-token']);
        console.log('data', data);
      } catch (err) {
        const mockCodes = [undefined, undefined, undefined, undefined];
        this.setState({ code: mockCodes });
        this.inputRef.clear();
        console.log('err', err.response);
        alert(err);
        return;
      }
      this.props.navigation.navigate('Tabs');
    }
  };
  toggleResend = () => {
    this.setState({ resendAvailable: !this.state.resendAvailable });
  };
  render() {
    const {
      step,
      formattedNumber,
      code,
      resendAvailable,
      isNumberInputFocused,
    } = this.state;
    return (
      <S.Container>
        {step === 0 && (
          <>
            <S.Title>{authStrings.title}</S.Title>
            <S.FieldName>{authStrings.telephoneNumber}</S.FieldName>
            <S.InputContainer isNumberInputFocused={isNumberInputFocused}>
              <S.PlaceHolder>+7</S.PlaceHolder>
              <TextInputMask
                onChangeText={async (formatted, extracted) => {
                  // this.setState({ extractedNumber: extracted });
                  this.setState({ formattedNumber: `+7 ${formatted}` });
                  if (extracted.length === 10) {
                    await http.post(
                      'http://g-service.dev-vps.ru/mobile/api/v1/cabinet/verify/phone-code',
                      {
                        phone: `7${extracted}`,
                      },
                    );
                    this.setState({ step: 1, extracted: `7${extracted}` });
                  }
                }}
                style={{
                  borderColor: '#8b8d8e',
                  borderBottomWidth: 1,
                  paddingLeft: 35,
                  fontSize: 24,
                }}
                onFocus={() => this.setState({ isNumberInputFocused: true })}
                autoFocus={true}
                keyboardType="number-pad"
                mask={'([000]) [000]-[00]-[00]'}
              />
            </S.InputContainer>
          </>
        )}
        {step === 1 && (
          <>
            <S.Title>{authStrings.codeTitle(formattedNumber)}</S.Title>
            <S.FieldName>Код из смс</S.FieldName>
            <S.CodeInputButton onPress={() => this.inputRef.focus()}>
              <S.CodeInputContainer>
                {code.map((field, index) =>
                  field === undefined ? (
                    <S.PassCodeChar isFirst={index === 0} />
                  ) : (
                    <S.CodeText isFirst={index === 0}>{field}</S.CodeText>
                  ),
                )}
              </S.CodeInputContainer>
            </S.CodeInputButton>
            <S.CodeInput
              // style={{ position: 'absolute', left: -10000 }}
              ref={ref => {
                this.inputRef = ref;
              }}
              underlineColorAndroid="transparent"
              blurOnSubmit={false}
              autoFocus={true}
              onChangeText={text => this.onInputChange(text)}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="number-pad"
              returnKeyType="next"
              // onSubmitEditing={this.submit.bind(this)}
            />
            <S.ResendButton
              disabled={!resendAvailable}
              onPress={async () => {
                await http.post(
                  'http://g-service.dev-vps.ru/mobile/api/v1/cabinet/verify/phone-code',
                  {
                    phone: this.state.extracted,
                  },
                );
              }}
              isResendAvailable={resendAvailable}>
              {!resendAvailable ? (
                <>
                  <S.ResendText isResendAvailable={resendAvailable}>
                    Отправить код повторно через 00:
                  </S.ResendText>
                  <CountDown
                    until={9}
                    onFinish={this.toggleResend}
                    size={10}
                    digitTxtStyle={{
                      color: 'black',
                      fontSize: 14,
                      fontWeight: '300',
                    }}
                    digitStyle={{
                      backgroundColor: 'rgba(252, 252, 252, 0.6)',
                      marginLeft: -3,
                    }}
                    timeToShow={['S']}
                    timeLabels={{ m: null, s: null }}
                    // style={{
                    //   position: 'absolute',
                    //   bottom: -40,
                    // }}
                  />
                </>
              ) : (
                <S.ResendText isResendAvailable={resendAvailable}>
                  Отправить код повторно
                </S.ResendText>
              )}
            </S.ResendButton>
            <S.ChangeNumberButton onPress={() => this.setState({ step: 0 })}>
              <S.ChangeNumberText>
                {authStrings.changeNumber}
              </S.ChangeNumberText>
            </S.ChangeNumberButton>
          </>
        )}
      </S.Container>
    );
  }
}

const mapDispatchToProps = {
  requestSaveToken,
};

export default connect(
  null,
  mapDispatchToProps,
)(Auth);
