import styled from 'styled-components';

export const Container = styled.ScrollView`
  background-color: #f4f6f7;
  flex: 1;
`;

export const Title = styled.Text`
  color: black;
  font-size: 18px;
  padding: 32px 40px 0 40px;
  text-align: center;
  font-weight: 700;
`;

export const FieldName = styled.Text`
  font-size: 14px;
  color: #1e1e1e;
  align-self: center;
  margin-top: 55px;
`;

export const InputContainer = styled.View`
  width: 100%;
  padding: 0 35px;
  opacity: ${({ isNumberInputFocused }) => (isNumberInputFocused ? 1 : 0.3)};
`;

export const PlaceHolder = styled.Text`
  font-size: 24px;
  position: absolute;
  left: 35px;
  bottom: 11;
`;

export const ResendButton = styled.TouchableOpacity`
  border-radius: 21px;
  border: 1px solid #e9530c;
  /* padding: 15px 30px; */
  background-color: ${({ isResendAvailable }) =>
    isResendAvailable ? '#e9530c' : 'transparent'};
  width: 290px;
  height: 42px;
  align-self: center;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const ResendText = styled.Text`
  font-size: 14px;
  color: ${({ isResendAvailable }) => (!isResendAvailable ? 'black' : 'white')};
`;

export const CodeInputButton = styled.TouchableWithoutFeedback``;

export const CodeInputContainer = styled.View`
  flex-direction: row;
  align-self: center;
  margin-top: 30px;
  margin-bottom: 40px;
  justify-content: center;
  align-items: center;
`;

export const PassCodeChar = styled.View`
  width: 8px;
  height: 8px;
  background-color: #d4d6d7;
  border-radius: 8px;
  margin-left: ${({ isFirst }) => (isFirst ? 0 : '22px')};
`;

export const CodeInput = styled.TextInput`
  position: absolute;
  left: -10000;
`;

export const CodeText = styled.Text`
  font-size: 14px;
  margin-left: ${({ isFirst }) => (isFirst ? 0 : '22px')};
`;

export const ChangeNumberButton = styled.TouchableOpacity`
  align-self: center;
  margin-top: 22px;
`;

export const ChangeNumberText = styled.Text`
  color: #e9530c;
  font-size: 14px;
  font-weight: 700;
`;
