import React from 'react';

import WebViewPage from '../WebViewPage';

const MapPage = () => {
  return (
    <WebViewPage
      title="Карта СТО"
      url="http://g-service.dev-vps.ru/mobile/ru-RU/map"
    />
  );
};

export default MapPage;
