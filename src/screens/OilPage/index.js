import React from 'react';

import WebViewPage from '../WebViewPage';

const OilPage = () => {
  return (
    <WebViewPage
      title="Подбор масла"
      url="http://g-service.dev-vps.ru/mobile/ru-RU/oil"
    />
  );
};

export default OilPage;
