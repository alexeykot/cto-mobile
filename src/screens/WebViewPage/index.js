import React from 'react';

// import Header from './components/Header';

import * as S from './styled';
import { ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';

import Header from './components/Header';

class WebViewPage extends React.Component {
  render() {
    const {
      // route: {
      //   params: { url, title },
      // },
      navigation,
      title,
      url,
    } = this.props;
    return (
      <S.Container>
        <Header
          // onBackPress={() => navigation.goBack()}
          // onHomePress={() => navigation.navigate('Main')}
          title={title}
          isHomeVisible={true}
          isMargined={true}
        />
        <WebView
          source={{ uri: url }}
          // startInLoadingState={true}
          // renderLoading={() => <ActivityIndicator style={{position: 'absolute',left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}} size="large" />}
        />
      </S.Container>
    );
  }
}

export default WebViewPage;
