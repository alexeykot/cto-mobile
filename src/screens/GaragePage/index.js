import React from 'react';
import { connect } from 'react-redux';

import WebViewPage from '../WebViewPage';

const GaragePage = props => {
  const { token } = props;
  return (
    <WebViewPage
      title="Мой гараж"
      url={`http://g-service.dev-vps.ru/mobile/ru-RU/cabinet/garage/index?access-token=${token}`}
    />
  );
};

const mapStateToProps = ({ auth }) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(GaragePage);
