import styled from 'styled-components';
import { Dimensions } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: white;
  align-items: center;
  padding: 0 10px;
`;

// export const Title = styled.Text`
//   color: #e9530c;
//   font-size: 24px;
//   text-align: center;
//   /* margin-top: 74px; */
// `;

export const PromoPicture = styled.Image`
  width: 100%;
  /* height: 50%; */
  height: ${Dimensions.get('window').height / 2.1}px;
`;

export const SkipContainer = styled.View`
  margin-top: 24px;
  /* margin-bottom: 37px; */
  margin-bottom: ${Dimensions.get('window').height / 35}px;
  padding-right: 20px;
  width: 100%;
  justify-content: flex-end;
  align-items: flex-end;
`;

export const SkipButton = styled.TouchableOpacity``;

export const SkipText = styled.Text`
  color: #8b8d8e;
  font-size: 14px;
`;

export const NextButton = styled.TouchableOpacity`
  width: 240px;
  height: 42px;
  background-color: #e9530c;
  border-radius: 21px;
  /* margin-top: 30px; */
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  text-transform: uppercase;
`;
