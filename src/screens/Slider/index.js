/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  Text,
} from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';

import * as S from './styled';
import http from '../../services/http';
import Title from './components/Title';

const styles = StyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    bottom: 0,
    left: 16,
    right: 16,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  paginationDots: {
    height: 16,
    margin: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 6,
    height: 6,
    borderRadius: 6,
    marginHorizontal: 7,
  },
});

export default class Slider extends React.Component {
  state = {
    slides: [],
  };
  async componentDidMount() {
    // const { data } = await http.get('/promo');
    // this.setState({ slides: data.data });
    let headers = {
      'Content-Type': 'application/json',
    };

    fetch('http://g-service.dev-vps.ru/mobile/api/v1/promo', {
      method: 'GET',
      headers: headers,
      body: null,
    })
      .then(response => response.json())
      .then(({ data }) => {
        this.setState({ slides: data });
      })
      .catch(error => console.log(error));
  }
  _renderItem = ({ item, index }) => {
    return (
      <S.Container>
        {/* <S.Title>{item.title}</S.Title> */}
        <S.SkipContainer>
          {index + 1 !== this.state.slides.length ? (
            <S.SkipButton onPress={this._onDone}>
              <S.SkipText>Пропустить</S.SkipText>
            </S.SkipButton>
          ) : null}
        </S.SkipContainer>
        <Title title={item.title} index={index} />
        <S.PromoPicture source={{ uri: item.image }} />
      </S.Container>
    );
  };
  _onDone = () => {
    this.props.navigation.navigate('Tabs');
  };
  onSliderChange = index => {
    if (index === this.state.slides.length) {
      this._onDone();
      return;
    }
    this._slider.goToSlide(index, true);
  };
  renderPagination = activeIndex => {
    return (
      <View style={styles.paginationContainer}>
        <S.NextButton onPress={() => this.onSliderChange(activeIndex + 1)}>
          <S.ButtonText>
            {activeIndex + 1 !== this.state.slides.length
              ? 'ДАЛЕЕ'
              : 'Поехали!'}
          </S.ButtonText>
        </S.NextButton>
        <SafeAreaView>
          <View style={styles.paginationDots}>
            {this.state.slides.length > 1 &&
              this.state.slides.map((_, i) => (
                <TouchableOpacity
                  key={i}
                  style={[
                    styles.dot,
                    i === activeIndex
                      ? { backgroundColor: '#e9530c' }
                      : { backgroundColor: 'rgba(0, 0, 0, .2)' },
                  ]}
                  onPress={() => this.onSliderChange(i)}
                />
              ))}
          </View>
        </SafeAreaView>
      </View>
    );
  };
  render() {
    return (
      <>
        {this.state.slides.length ? (
          <AppIntroSlider
            ref={ref => (this._slider = ref)}
            renderItem={this._renderItem}
            data={this.state.slides}
            onDone={this._onDone}
            renderPagination={this.renderPagination}
            showNextButton={false}
            showDoneButton={false}
          />
        ) : null}
      </>
    );
  }
}
