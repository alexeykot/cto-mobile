import styled from 'styled-components';
import { Dimensions } from 'react-native';

export const Container = styled.View`
  /* margin-top: 74px; */
  /* margin-bottom: 15%; */
  margin-bottom: ${Dimensions.get('window').height / 25}px;
  text-align: center;
  flex-wrap: nowrap;
  /* min-height: 90px; */
  min-height: ${Dimensions.get('window').height / 6}px;
`;

export const PrimaryText = styled.Text`
  color: #e9530c;
  font-size: 24px;
  text-align: center;
`;

export const SecondaryText = styled.Text`
  color: black;
  font-size: 24px;
  text-align: center;
`;
