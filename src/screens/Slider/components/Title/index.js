import React from 'react';

import * as S from './styled';

const strings = [
  {
    primary: 'G-Energy Service',
    secondary: 'формула удобного обслуживания!',
  },
  {
    primary: 'Просматривайте историю обслуживания',
    secondary: 'своих автомобилей',
  },
  {
    primary: 'Копите баллы',
    secondary: 'и получайте скидки',
  },
  {
    primary: 'Подбирайте лучшее масло',
    secondary: 'для своего автомобиля',
  },
  {
    primary: 'Узнавайте о новых акциях',
    secondary: 'первыми',
  },
  {
    primary: 'Станьте нашим клиентом и получите',
    secondary: 'максимум возможностей',
  },
];

const Title = ({ title, index }) => {
  const splitTitle = () => {
    // const strArr = title.split(' ');
    switch (index) {
      case '0':
        return (
          <>
            <S.PrimaryText>G-Energy Service</S.PrimaryText>
            <S.SecondaryText>формула удобного обслуживания!</S.SecondaryText>
          </>
        );
      default:
        return (
          <>
            <S.PrimaryText>G-Energy Service</S.PrimaryText>
            <S.SecondaryText>формула удобного обслуживания!</S.SecondaryText>
          </>
        );
    }
  };
  return (
    <S.Container>
      <S.PrimaryText>{strings[index].primary}</S.PrimaryText>
      <S.SecondaryText>{strings[index].secondary}</S.SecondaryText>
    </S.Container>
  );
};

export default Title;
