import React from 'react';

import WebViewPage from '../WebViewPage';

const ServicePage = () => {
  return (
    <WebViewPage
      title="Запись на СТО"
      url="http://g-service.dev-vps.ru/mobile/ru-RU/record"
    />
  );
};

export default ServicePage;
