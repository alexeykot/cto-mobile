import axios from 'axios';

const baseUrl = 'http://g-service.dev-vps.ru/api/v1';

const http = axios.create({
  withCredentials: false,
  baseUrl,
  headers: {'Content-Type': 'application/json'},
});

export default http;
