import { takeEvery } from 'redux-saga/effects';
const SAVE_TOKEN = 'auth/SAVE_TOKEN';

export const initialState = {
  // token: 'OZ30_rYUCkL7-TR2U2guKmnYc91wVuJIDowqVtRu4eBekIgkghpeqHBRbsmnDSp1S9-tV1_PSr8zzQworgLj8zPwV3fVzyPNZI5N6h1nisvoTM4-EX6_9O_VShLP1CCB',
  token: '',
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SAVE_TOKEN:
      return { ...state, token: action.payload };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestSaveToken = token => ({
  type: SAVE_TOKEN,
  payload: token,
});

// <<<WORKERS>>>
function* signUp({ payload }) {
  try {
    console.log(payload);
  } catch (err) {
    console.log('err');
  }
}

// <<<WATCHERS>>>
export function* watchSignUp() {
  yield takeEvery('SIGN_UP', signUp);
}
