export default {
  title:
    'Для входа в ваш гараж введите номер телефона, указанный при обращении в G-Energy Service',
  telephoneNumber: 'Номер телефона',
  codeTitle: number =>
    `Пароль к вашему гаражу вы можете найти в СМС на номер ${number} от отправителя G-Energy`,
  changeNumber: 'Изменить номер телефона',
};
