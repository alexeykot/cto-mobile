import React from 'react';

import AppIntroSlider from 'react-native-app-intro-slider';

import {View, StyleSheet, Text} from 'react-native';

const styles = StyleSheet.create({
  slite: {
    flex: 1,
  },
});

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    backgroundColor: '#59b2ab',
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Other cool stuff',
    backgroundColor: '#febe29',
  },
  {
    key: 'somethun1',
    title: 'Rocket guy',
    text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
    backgroundColor: '#22bcb5',
  },
];

export default class App extends React.Component {
  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text>{item.title}</Text>
        <Text>{item.text}</Text>
      </View>
    );
  };
  // _onDone = () => {
  //   // User finished the introduction. Show real app through
  //   // navigation or simply by controlling state
  //   this.setState({showRealApp: true});
  // };
  render() {
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        slides={slides}
        onDone={this._onDone}
      />
    );
  }
}
